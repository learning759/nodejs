// MODULES !!!!

// CommonJS , Every file in module(by default)
// Modules - Encapsulated Code (Only share minimum)
// every file in node is a module


const names = require('./4-firstModule');
const sayHi = require('./5-secondModule');
require('./7-module')

sayHi(names.nevin)
sayHi(names.kevin)

