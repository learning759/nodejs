const _ = require('lodash');

const inpArray = [1, [2, [3, [4, [5]]]]];
// flatten the inpArray
const newArray = _.flattenDeep(inpArray);
console.log(newArray);