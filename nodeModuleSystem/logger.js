console.log('FileName ' + __filename) //__filename and __dirname are arguments of module wrapper fuction
console.log('DirName ' + __dirname)


var URL = 'http://mylogger.io/log';

function log(message) {
    // send an HTTP request
    console.log(message)
}

// module.exports.log = log; // Exports the funtion as object
module.exports = log; // exports the single function only