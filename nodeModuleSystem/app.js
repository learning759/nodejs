// Global Objects

// console.log(); // Here console is a global object, Which means we can access it from anywhere in any files

// setTimeout(); // Also a Global Object, used to call a fucntion after a delay
// clearTimeout();

// setInterval(); //  A Global object, used to repeatedly call a function after a given delay
// clearInterval();

//In Node js there are lot of  global objects


// In JavaScript window is used for getting global objects
// eg: window.console.log()

// In Node js Instead of window we have global
// eg: global.console.log()
// but we can use short hand of global objects like console.log()

// variable declaration
// In JS var message = 'Nevin'; --> it is defined globaly, we can take value as window.message
// IN Js var message = 'Nevin'; --> it is not difined as globaly
// eg
// var message = 'nevin'
// console.log(global.message) // result ==> undefined

// variables or fucntions defined here are not added to global objects they are only scoped to this file app.js this is beacuase of node modular system



// console.log(module) // In node every file is a module, A Node contains atleast one module, Here the app.js is main module and the variables and function defined in that file are scoped to that module

// //creating and load a module


// const logger = require('./logger');
// console.log(logger)
// logger.log('Message') // for exporting funtion as object
// logger('message') // for exporting a single fucntion

// Every module/file is wrapped inside a function called module wrapper function

// (function (exports, require, module, __filename, __dirname) {
//     var URL = 'http://logger.io/log'
//     function log(message) {
//         console.log(message)
//     }
//  module.exports = log;
// })

// the syntax of module wrapper function is shown above


const path = require('path')

var pathObj = path.parse(__filename);
console.log(pathObj)

const os = require('os')

var totalMemory = os.totalmem();
var freeMemory = os.freemem();

console.log('Total Memory ' + totalMemory)
console.log('Free Memory ' + freeMemory)


// file system
const fs = require('fs')

var files = fs.readdirSync('./');
console.log(files)